#!/usr/bin/php
<?php
while (1)
{
	print("Enter a number: ");
	$num = trim(fgets(STDIN));
	if (feof(STDIN))
		break;
	if(is_numeric($num))
	{
		if ($num % 2 == 0 || $num == 0)
			print("The number $num is even\n");
		else
			print("The number $num is odd\n");
	}
	else
		echo "'$num' is not a number\n";
}
